async function getAll() {
  try {
      const posts = await axios.get('https://jsonplaceholder.typicode.com/posts');
      const users = await axios.get('https://jsonplaceholder.typicode.com/users');

      for (let count = 0; count < posts.data.length; count++) {
          const postUserId = posts.data[count].userId;
          const userData = users.data.find(user => user.id === postUserId);
          posts.data[count].user = userData; 
      }

      console.log(posts.data)
  } catch (error) {
      
  }
}

getAll()